#!/bin/bash

# Get current time
time=$(date +"%-H:%M")
day=$(date +"%d")
month=$(date +"%m")
year=$(date +"%Y")

# Set file name
filename="syslog-$time-$day-$month-$year.txt"

# Encrypt function
function encrypt() {
    local str="$1"
    local offset=$(date +"%-H")
    local result=""
    for (( i=0; i<${#str}; i++ )); do
        char="${str:i:1}"
        if [[ "$char" =~ [a-zA-Z] ]]; then
            if [[ "$char" =~ [A-Z] ]]; then
                ascii=$(( $(printf '%d' "'$char") + offset - 65 ))
                ascii=$(( (ascii % 26) + 65 ))
                char=$(printf "\\$(printf '%03o' "$ascii")")
            else
                ascii=$(( $(printf '%d' "'$char") + offset - 97 ))
                ascii=$(( (ascii % 26) + 97 ))
                char=$(printf "\\$(printf '%03o' "$ascii")")
            fi
        fi
        result="$result$char"
    done
    echo "$result"
}

# Create backup and encrypt file
cp /var/log/syslog "$filename"
encrypted=$(encrypt "$(cat "$filename")")
echo "$encrypted" > "$filename"

# Move backup file to backup directory
mkdir -p ~/backup
mv "$filename" ~/backup/

#crontab
#59 23 * * * /Home/kali/Documents/PraktikumSISOP/Modul1/soal4/encrypt.sh
