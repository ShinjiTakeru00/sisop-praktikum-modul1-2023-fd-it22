#!/bin/bash

# Get file name as argument
filename="$1"

# Decrypt function
function decrypt() {
    local str="$1"
    local offset=$(date +"%H")
    local result=""
    for (( i=0; i<${#str}; i++ )); do
        char="${str:i:1}"
        if [[ "$char" =~ [a-zA-Z] ]]; then
            if [[ "$char" =~ [A-Z] ]]; then
                ascii=$(( $(printf '%d' "'$char") - offset + 65 ))
                ascii=$(( (ascii % 26) + 65 ))
                char=$(printf "\\$(printf '%03o' "$ascii")")
            else
                ascii=$(( $(printf '%d' "'$char") - offset + 97 ))
                ascii=$(( (ascii % 26) + 97 ))
                char=$(printf "\\$(printf '%03o' "$ascii")")
            fi
        fi
        result="$result$char"
    done
    echo "$result"
}

# Decrypt file and overwrite it
decrypted=$(decrypt "$(cat "$filename")")
echo "$decrypted" > "$filename"

