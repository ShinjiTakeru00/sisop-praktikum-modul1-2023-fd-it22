# sisop-praktikum-modul1-2023-FD-IT22



## Anggota Kelompok IT22

Evan Darya Kusuma (5027211069)

## Nomor 1

**Analisa Soal**

Kita perlu melakukan filtering file 2023 QS World University Ranking sesuai dengan poin - poin yang ada di soal yaitu :
- Menampilkan top 5 universitas jepang
- Mengurutkan 5 universitas jepang yang memiliki frs paling rendah diantara yang paling tinggi
- Menampilkan 10 universitas jepang yang punya GER rank paling tinggi
- Menampilkan universitas dengan kode keren

**Code**
```
#!/bin/bash

univ_survey=$(cat '2023 QS World University Rankings.csv')

# poin 1
univ_best=$(echo "$univ_survey" | awk -F, '{ if ($3 == "JP") print $0 }' | sort -n -t ',' -k1 | head -n 5)
echo "Top 5 Universitas yang ada di Jepang:"
echo "$univ_best"  | awk -F, '{ print $1 "\t" $2 }'
echo " "

# poin 2
fsr_terendah=$(echo "$univ_best" | sort -n -t ',' -k9 | tail -n 5)
echo "Universitas dengan socre FRS terendah di antara 5 Universitas Jepang teratas:"
echo "$fsr_terendah" | awk -F, '{ print $2 "\t" $9 }' 
echo " "

# poin 3
ger_best=$(echo "$univ_survey" | awk -F, '{ if ($3 == "JP") print $0 }' | sort -n -t ',' -k20 | tail -n 10)
echo "Top 10 Universitas yang ada di Jepang yang mempunyai GER Rank tertinggi:"
echo "$ger_best" | awk -F, '{ print $1 "\t" $2 "\t" $20 }'
echo " "

# poin 4
univ_keren=$(echo "$univ_survey" | grep -i "keren")
echo "Universitas paling keren di dunia:"
echo "$univ_keren" | awk -F, '{ print $1 "\t" $2 }'

```

**Penjelasan Code**

Pada poin 1, script membaca isi file CSV dan menyimpannya ke dalam variabel univ_survey. Kemudian, menggunakan command awk, script memfilter baris-baris dari univ_survey yang memiliki kode negara "JP" (kode negara untuk Jepang) di kolom ke-3, dan menyimpan hasilnya dalam variabel univ_best. Setelah itu, script menampilkan 5 universitas terbaik yang ada di Jepang dengan menampilkan kolom 1 (peringkat) dan kolom 2 (nama universitas) dari univ_best.

Pada poin 2, script menggunakan command sort untuk mengurutkan univ_best berdasarkan score FSR (kolom ke-9) secara ascending, dan menyimpan 5 universitas dengan score FSR terendah ke dalam variabel fsr_terendah. Kemudian, script menampilkan universitas-universitas tersebut dengan menampilkan kolom 2 (nama universitas) dan kolom 9 (score FSR) dari fsr_terendah.

Pada poin 3, script menggunakan command sort untuk mengurutkan univ_survey berdasarkan GER Rank (kolom ke-20) secara descending, dan menyimpan 10 universitas terbaik yang ada di Jepang ke dalam variabel ger_best. Kemudian, script menampilkan 10 universitas tersebut dengan menampilkan kolom 1 (peringkat), kolom 2 (nama universitas), dan kolom 20 (GER Rank) dari ger_best.

Pada poin 4, script menggunakan command grep untuk mencari baris-baris yang mengandung kata "keren" dalam univ_survey, dan menyimpan hasilnya ke dalam variabel univ_keren. Kemudian, script menampilkan universitas paling keren di dunia dengan menampilkan kolom 1 (peringkat) dan kolom 2 (nama universitas) dari univ_keren.


## Nomor 2

**Analisa Soal**

Kita diinstruksikan untuk membuat script download gambar tentang Indonesia sesuai waktu yang ada (misalkan jam 12, jadi 12 gambar akan didownload pada saat itu dan terkumpul dalam satu folder). Gambar diunggah setiap 10 jam. Itu kemudian dikzip sekali sehari untuk menghemat memori. 

**Code**
```
#!/bin/bash

# download gambar dari situs unsplash.com
download_image() {
    local number="$1"
    wget "https://unsplash.com/s/photos/indonesia?orientation=landscape&page=$number" -O - \
    | grep -oP 'https://images.unsplash.com/photo-[^\?]*\?'
}

# buat folder dan nama file gambar
make_folder_and_filename() {
    local no_folder="$1"
    local no_file="$2"
    local nama_folder="kumpulan_$no_folder"
    local nama_file="perjalanan_$no_file.jpg"
    mkdir -p "$nama_folder"
    echo "$nama_folder/$nama_file"
}

# men-zip folder kumpulan
zip_folder() {
    local no_folder="$1"
    local nama_folder="kumpulan_$no_folder"
    zip -r "devil_$no_folder.zip" "$nama_folder"
}

# Nyari jam sekarang
hour=$(date +%H)

# nomor folder dan nomor file
no_folder=1
no_file=1

while true
do
    if [ "$hour" -eq "0" ]; then
        # Jika jam 00:00, download satu gambar saja
        nama_file=$(make_folder_and_filename $no_folder $no_file)
        download_image 1 > $nama_file
        echo "Downloaded $nama_file"
        no_file=$((no_file+1))
    else
        # Jika jam bukan 00:00, download gambar sebanyak jam sekarang
        for i in $(seq 1 $hour); do
            nama_file=$(make_folder_and_filename $no_folder $no_file)
            download_image $i > $nama_file
            echo "Downloaded $nama_file"
            no_file=$((no_file+1))
        done
    fi

    # Zip folder tiap 1 hari
    current_date=$(date +%Y%m%d)
    if [ "$previous_date" != "$current_date" ]; then
        zip_folder $no_folder
        echo "Folder kumpulan_$no_folder zipped"
        no_folder=$((no_folder+1))
    fi

    previous_date="$current_date"

    # Tunggu 10 jam sebelum download lagi
    sleep 36000
done

```

**Penjelasan Code**

`#!/bin/bash` - tanda shebang untuk memberitahu sistem bahwa script ini harus dijalankan dengan menggunakan bash shell.

`download_image()` - sebuah fungsi yang digunakan untuk mendownload gambar dari situs unsplash.com. Fungsi ini menerima satu parameter yaitu nomor halaman. Fungsi ini menggunakan perintah wget untuk mengunduh halaman situs unsplash.com yang mengandung gambar dengan kata kunci "indonesia" dan orientasi landscape pada halaman tersebut. Kemudian, perintah grep digunakan untuk mencari URL gambar pada halaman tersebut.

`make_folder_and_filename()` - sebuah fungsi yang digunakan untuk membuat folder dan nama file gambar. Fungsi ini menerima dua parameter yaitu nomor folder dan nomor file. Fungsi ini membuat nama folder "kumpulan_$no_folder" dan nama file "perjalanan_$no_file.jpg" dengan menggunakan perintah mkdir dan echo.

`zip_folder()` - sebuah fungsi yang digunakan untuk men-zip folder kumpulan. Fungsi ini menerima satu parameter yaitu nomor folder. Fungsi ini menggunakan perintah zip untuk men-zip folder "kumpulan_$no_folder" ke dalam file "devil_$no_folder.zip".

`hour=$(date +%H)` - sebuah perintah yang digunakan untuk mendapatkan jam sekarang pada sistem.

`no_folder=1` dan `no_file=1` - variabel yang digunakan untuk menyimpan nomor folder dan nomor file.

`while true` - sebuah loop yang digunakan untuk melakukan download gambar secara berulang-ulang.

`if [ "$hour" -eq "0" ]; then` - sebuah percabangan yang digunakan untuk mengecek apakah jam sekarang adalah 00:00. Jika ya, maka hanya satu gambar yang didownload.



## Nomor 3

**Analisa Soal**

diperintahkan untuk membuat dua skrip. Skrip pertama bernama louis.sh berisi skrip pendaftaran akun dengan username dan password yang dimasukkan. Kata sandi yang dimasukkan harus memenuhi kriteria pertanyaan. Setiap akun yang berhasil didaftarkan dimasukkan ke dalam file users.txt. Skrip satunya bernama retep.sh berisi skrip login untuk akun tersebut. Kami juga diinstruksikan untuk menulis registrasi pengguna dan status login ke file log.txt. 

**Code**
louis.sh
```
#!/bin/bash

# Untuk membuat log dan folder users jika belum ada
if [ ! -f log.txt ]; then
	touch log.txt
fi

if [ ! -d  ./users ]; then
	mkdir users
fi

if [ ! -f ./users/users.txt ]; then
	touch ./users/users.txt 
fi


# Mendapatkan hari bulan tanggal waktu
datetime=$(date "+%y/%m/%d %H:%M:%S")

# Input username
read -p "Username: " username

# Validasi username
while grep -q "^$username:" ./users/users.txt; do
	echo "$datetime REGISTER: ERROR User already exists" >> log.txt
  echo "Username already exists"
	exit
done

# Input password
read -s -p "Password: " password

# Validasi password
while true; do 
  if [[ ${#password} -lt 8 ]]; then
    read -s -p "Password is too short. Enter a password (minimum 8 characters): " password
    echo ""
  elif ! [[ "$password" =~ [A-Z] ]] || ! [[ "$password" =~ [a-z] ]] || ! [[ "$password" =~ [0-9] ]]; then
    read -s -p "Password must be alphanumeric with at least one uppercase and one lowercase letter. Enter a password: " password
    echo ""
  elif [[ "$password" == *"$username"* ]]; then
    read -s -p "Password cannot contain username. Enter a password: " password
    echo ""
  elif [[ "$password" == *"chicken"* || "$password" == *"ernie"* ]]; then
    read -s -p "Password cannot contain 'chicken' or 'ernie'. Enter a password: " password
    echo ""
  else
    # Menambahkan data user ke dalam file /users/users.txt
    echo "$username:$password" >> ./users/users.txt
    echo "$datetime REGISTER: INFO User $username registered successfully" >> log.txt
    echo "User '$username' registered successfully."
    break
  fi
done
```

retep.sh
```
#!/bin/bash

# Membuat file log.txt jika belum ada
touch log.txt

# Meminta input username dari pengguna
read -p "Masukkan username: " username

# Mencari informasi username dan password dari file /users/users.txt
user_info=$(grep "^$username:" ./users/users.txt)

# Memeriksa apakah username ada di dalam file /users/users.txt
if [ -z "$user_info" ]; then
    echo "Username tidak terdaftar."
    echo "$(date +'%y/%m/%d %H:%M:%S') LOGIN: ERROR Failed login attempt on user $username" >> log.txt
    exit 1
fi

# Memeriksa apakah password cocok dengan yang ada di dalam file /users/users.txt
read -s -p "Masukkan password: " password
if [ "$password" != "$(echo $user_info | cut -d':' -f2)" ]; then
    echo "Password salah."
    echo "$(date +'%y/%m/%d %H:%M:%S') LOGIN: ERROR Failed login attempt on user $username" >> log.txt
    exit 1
fi

# Jika username dan password cocok, maka tampilkan pesan sukses dan catat ke dalam file log.txt
echo "Login berhasil."
echo "$(date +'%y/%m/%d %H:%M:%S') LOGIN: INFO User $username logged in" >> log.txt

```

## Nomor4

**Analisa Soal**

Kita perlu melakukan filtering file 2023 QS World University Ranking sesuai dengan poin - poin yang ada di soal yaitu :
- Menampilkan top 5 universitas jepang
- Mengurutkan 5 universitas jepang yang memiliki frs paling rendah diantara yang paling tinggi
- Menampilkan 10 universitas jepang yang punya GER rank paling tinggi
- Menampilkan universitas dengan kode keren

**Code**
encrypt.sh
```
#!/bin/bash

# Get current time
time=$(date +"%-H:%M")
day=$(date +"%d")
month=$(date +"%m")
year=$(date +"%Y")

# Set file name
filename="syslog-$time-$day-$month-$year.txt"

# Encrypt function
function encrypt() {
    local str="$1"
    local offset=$(date +"%-H")
    local result=""
    for (( i=0; i<${#str}; i++ )); do
        char="${str:i:1}"
        if [[ "$char" =~ [a-zA-Z] ]]; then
            if [[ "$char" =~ [A-Z] ]]; then
                ascii=$(( $(printf '%d' "'$char") + offset - 65 ))
                ascii=$(( (ascii % 26) + 65 ))
                char=$(printf "\\$(printf '%03o' "$ascii")")
            else
                ascii=$(( $(printf '%d' "'$char") + offset - 97 ))
                ascii=$(( (ascii % 26) + 97 ))
                char=$(printf "\\$(printf '%03o' "$ascii")")
            fi
        fi
        result="$result$char"
    done
    echo "$result"
}

# Create backup and encrypt file
cp /var/log/syslog "$filename"
encrypted=$(encrypt "$(cat "$filename")")
echo "$encrypted" > "$filename"

# Move backup file to backup directory
mkdir -p ~/backup
mv "$filename" ~/backup/

#crontab
#59 23 * * * /Home/kali/Documents/PraktikumSISOP/Modul1/soal4/encrypt.sh

```

decrypt.sh
```
#!/bin/bash

# Get file name as argument
filename="$1"

# Decrypt function
function decrypt() {
    local str="$1"
    local offset=$(date +"%H")
    local result=""
    for (( i=0; i<${#str}; i++ )); do
        char="${str:i:1}"
        if [[ "$char" =~ [a-zA-Z] ]]; then
            if [[ "$char" =~ [A-Z] ]]; then
                ascii=$(( $(printf '%d' "'$char") - offset + 65 ))
                ascii=$(( (ascii % 26) + 65 ))
                char=$(printf "\\$(printf '%03o' "$ascii")")
            else
                ascii=$(( $(printf '%d' "'$char") - offset + 97 ))
                ascii=$(( (ascii % 26) + 97 ))
                char=$(printf "\\$(printf '%03o' "$ascii")")
            fi
        fi
        result="$result$char"
    done
    echo "$result"
}

# Decrypt file and overwrite it
decrypted=$(decrypt "$(cat "$filename")")
echo "$decrypted" > "$filename"

```

