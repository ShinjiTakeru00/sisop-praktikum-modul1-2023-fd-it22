#!/bin/bash

# download gambar dari situs unsplash.com
download_image() {
    local number="$1"
    wget "https://unsplash.com/s/photos/indonesia?orientation=landscape&page=$number" -O - \
    | grep -oP 'https://images.unsplash.com/photo-[^\?]*\?'
}

# buat folder dan nama file gambar
make_folder_and_filename() {
    local no_folder="$1"
    local no_file="$2"
    local nama_folder="kumpulan_$no_folder"
    local nama_file="perjalanan_$no_file.jpg"
    mkdir -p "$nama_folder"
    echo "$nama_folder/$nama_file"
}

# men-zip folder kumpulan
zip_folder() {
    local no_folder="$1"
    local nama_folder="kumpulan_$no_folder"
    zip -r "devil_$no_folder.zip" "$nama_folder"
}

# Nyari jam sekarang
hour=$(date +%H)

# nomor folder dan nomor file
no_folder=1
no_file=1

while true
do
    if [ "$hour" -eq "0" ]; then
        # Jika jam 00:00, download satu gambar saja
        nama_file=$(make_folder_and_filename $no_folder $no_file)
        download_image 1 > $nama_file
        echo "Downloaded $nama_file"
        no_file=$((no_file+1))
    else
        # Jika jam bukan 00:00, download gambar sebanyak jam sekarang
        for i in $(seq 1 $hour); do
            nama_file=$(make_folder_and_filename $no_folder $no_file)
            download_image $i > $nama_file
            echo "Downloaded $nama_file"
            no_file=$((no_file+1))
        done
    fi

    # Zip folder tiap 1 hari
    current_date=$(date +%Y%m%d)
    if [ "$previous_date" != "$current_date" ]; then
        zip_folder $no_folder
        echo "Folder kumpulan_$no_folder zipped"
        no_folder=$((no_folder+1))
    fi

    previous_date="$current_date"

    # Tunggu 10 jam sebelum download lagi
    sleep 36000
done

